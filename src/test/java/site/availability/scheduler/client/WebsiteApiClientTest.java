package site.availability.scheduler.client;

import java.util.Arrays;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.web.client.RestTemplate;

@RunWith(MockitoJUnitRunner.class)
public class WebsiteApiClientTest {


  @Mock
  private RestTemplate mockRestTemplate;
  @InjectMocks
  private WebsiteApiClient websiteApiClient;


  @Test
  @Ignore
  public void test_get_websites_url_build_correctly() {
    //given
    websiteApiClient.setWebsiteApiUrl("v1/sa/website");

    //when
    websiteApiClient.getWebsites(10, 1, Arrays.asList("f1", "f2"));

    //then
  }

}