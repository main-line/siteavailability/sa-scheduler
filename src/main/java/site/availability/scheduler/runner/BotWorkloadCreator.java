package site.availability.scheduler.runner;

import com.amazonaws.services.s3.AmazonS3;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.time.Instant;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import javax.annotation.Resource;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Component;
import site.availability.scheduler.client.WebsiteApiClient;
import site.availability.scheduler.model.BotWorkload;
import site.availability.scheduler.model.PageResult;
import site.availability.scheduler.model.Website;

@Component
@Slf4j
@ConditionalOnProperty(value = "jobs.enabled", matchIfMissing = true, havingValue = "true")
public class BotWorkloadCreator {

  @Resource
  private AmazonS3 amazonS3;
  @Resource
  private WebsiteApiClient websiteApiClient;
  @Resource
  private ObjectMapper objectMapper;

  @Value("${sa.bucket.name}")
  @Setter
  private String bucketName;

  public void schedule() throws JsonProcessingException {
    int pageSize = 2000;
    int page = 0;
    String keyPrefix = Instant.now().toString();
    PageResult<Website> results = null;
    do {
      results = websiteApiClient.getWebsites(pageSize, page, Arrays.asList("url", "redirect"));
      if (results.getContent().size() != 0) {
        String keyName = String.format("in/%s/pageSize=%s&page=%s", keyPrefix, pageSize, page);
        amazonS3.putObject(bucketName.trim(), keyName,
            objectMapper.writeValueAsString(buildWorkLoads(results.getContent())));
      }
      page++;
    } while (results.getContent().size() != 0);
  }

  private List<BotWorkload> buildWorkLoads(List<Website> websites) {
    return websites.stream()
        .map(website -> new BotWorkload(website.getUrl(), website.getRedirect()))
        .collect(
            Collectors.toList());
  }
}
