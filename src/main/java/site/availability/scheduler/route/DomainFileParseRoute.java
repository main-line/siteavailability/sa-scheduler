package site.availability.scheduler.route;

import javax.annotation.Resource;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.model.dataformat.CsvDataFormat;
import org.apache.camel.processor.aggregate.GroupedBodyAggregationStrategy;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import site.availability.scheduler.processor.CsvToWebsiteBeanProcessor;

@Component
public class DomainFileParseRoute extends RouteBuilder {

  @Value("${sa.data.parse.file.path}")
  private String fileComponentPath;

  @Value("${sa.data.parse.websites.per.http.request}")
  private Integer linesCount;

  @Resource
  private CsvToWebsiteBeanProcessor csvToWebsiteBeanProcessor;

  @Override
  public void configure() throws Exception {
    CsvDataFormat csv = new CsvDataFormat();
    csv.setSkipHeaderRecord(true);
    csv.setDelimiter("\t");
    csv.setLazyLoad(true);

    from(fileComponentPath)
        .unmarshal(csv)
        .split(body()).streaming()
        .aggregate(constant(true), new GroupedBodyAggregationStrategy())
        .completionSize(linesCount)
        .completionTimeout(5000)
        .bean(csvToWebsiteBeanProcessor);

  }
}
