package site.availability.scheduler.processor;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import javax.annotation.Resource;
import org.springframework.stereotype.Component;
import site.availability.scheduler.client.WebsiteApiClient;
import site.availability.scheduler.model.SourceType;
import site.availability.scheduler.model.Website;

@Component
public class CsvToWebsiteBeanProcessor {

  @Resource
  private WebsiteApiClient websiteApiClient;

  public void handle(List<List<String>> websitesCsvData) {
    List<Website> websiteList = websitesCsvData
        .stream()
        .map(this::buildWebsiteModelToSave)
        .collect(Collectors.toList());
    websiteApiClient.putWebsites(websiteList);
  }


  private static String reverseDomainNameString(List<String> brokenUrlComponents) {
    Collections.reverse(brokenUrlComponents);
    return String.join(".", brokenUrlComponents.toArray(new String[0]));
  }

  private Website buildWebsiteModelToSave(List<String> websiteData) {
    List<String> brokenUrlComponents = Arrays.asList(websiteData.get(4).split("\\."));
    Website website = new Website();
    website.setTld(brokenUrlComponents.get(0));
    website.setUrl(reverseDomainNameString(brokenUrlComponents));
    website.setSource(SourceType.COMMON_CRAWL);
    website.setActive(true);
    return website;
  }

}
