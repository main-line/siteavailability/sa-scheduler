package site.availability.scheduler.client;

import java.util.List;
import javax.annotation.Resource;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;
import site.availability.scheduler.model.PageResult;
import site.availability.scheduler.model.Website;

@Component
@Slf4j
public class WebsiteApiClient {

  @Value("${sa.api.website.url}")
  @Setter
  private String websiteApiUrl;

  @Resource
  private RestTemplate restTemplate;

  public PageResult<Website> getWebsites(Integer pageSize, Integer page, List<String> fields) {
    String requestUrl = UriComponentsBuilder.fromHttpUrl(websiteApiUrl)
        .queryParam("fields", fields.toArray())
        .queryParam("pageSize", pageSize)
        .queryParam("page", page).toUriString();
    log.info("Request url : {}", requestUrl);
    return restTemplate.exchange(requestUrl, HttpMethod.GET, null,
        new ParameterizedTypeReference<PageResult<Website>>() {
        }).getBody();

  }

  public void putWebsites(List<Website> websites) {
    log.info("Persisting websites to db : {}", websites.size());
    restTemplate.put(websiteApiUrl + "/bulk", websites);
  }

}
