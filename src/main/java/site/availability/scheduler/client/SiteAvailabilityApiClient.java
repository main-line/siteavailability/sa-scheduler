package site.availability.scheduler.client;

import java.util.List;
import javax.annotation.Resource;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import site.availability.scheduler.model.WebsiteAvailability;

@Component
@Slf4j
public class SiteAvailabilityApiClient {

  @Value("${sa.availability.timeseries.url}")
  private String timeSeriesHostUrl;

  @Resource
  private RestTemplate restTemplate;

  public void timeSeriesPost(List<WebsiteAvailability> websiteAvailabilities) {
    restTemplate.put(timeSeriesHostUrl, websiteAvailabilities);
    log.info("Posted availabilites to api Size : {}", websiteAvailabilities.size());
  }


}
