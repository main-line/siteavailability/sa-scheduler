package site.availability.scheduler.model;

import lombok.Data;

@Data
public class BotWorkload {

  private String url;
  private String redirect;

  public BotWorkload(String url, String redirect) {
    this.url = url;
    this.redirect = redirect;
  }

}
