package site.availability.scheduler.model;

import java.util.List;
import lombok.Data;

@Data
public class PageResult<T> {

  private List<T> content;
  private long totalCount;
  private Integer pageSize;
  private Integer page;

  public PageResult<T> of(List<T> content) {
    this.content = content;
    return this;
  }

  public PageResult<T> totalCount(long totalCount) {
    this.totalCount = totalCount;
    return this;
  }

  public PageResult<T> pageSize(Integer pageSize) {
    this.pageSize = pageSize;
    return this;
  }

  public PageResult<T> page(Integer page) {
    this.page = page;
    return this;
  }
}
