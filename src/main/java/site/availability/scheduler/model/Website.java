package site.availability.scheduler.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import lombok.Data;

@Data
@JsonInclude(value = Include.NON_NULL)
public class Website {

  private String url;
  private SourceType source;
  private Boolean active;
  private String redirect;
  private String tld;
  ;
}
