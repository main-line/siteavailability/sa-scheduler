package site.availability.scheduler.model;

public enum SourceType {
  TOPM, SEARCH, COMMON_CRAWL
}
