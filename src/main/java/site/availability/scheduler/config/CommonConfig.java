package site.availability.scheduler.config;

import com.amazonaws.services.sqs.AmazonSQSAsync;
import javax.annotation.Resource;
import org.springframework.cloud.aws.messaging.core.QueueMessagingTemplate;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

@Configuration
public class CommonConfig {

  @Resource
  private AmazonSQSAsync amazonSQSAsync;

  @Bean
  public RestTemplate restTemplate() {
    return new RestTemplate();
  }

  @Bean
  public QueueMessagingTemplate queueMessagingTemplate() {
    return new QueueMessagingTemplate(amazonSQSAsync);
  }
}
